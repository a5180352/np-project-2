#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>

// errexit - print an error message and exit
int errexit(char *format, ...) {
    va_list	args;
    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);
    exit(1);
}

int serv_tcp_sock(int port_num, int qlen) {
// int port_num : service associated with the desired port
// int qlen;    : maximum length of the server request queue
    struct sockaddr_in sin; // an Internet endpoint address
    int sock;               // socket descriptor and socket type
    bzero((char *)&sin, sizeof(sin));
    sin.sin_family = AF_INET;
    // sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_addr.s_addr = inet_addr("127.0.0.1");
    // Map service name to port number
    if ((sin.sin_port = htons((u_short)port_num)) == 0)
        errexit("can't get \"%d\" service entry\n", port_num);
    // Allocate a socket
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        errexit("can't create socket: %s\n", strerror(errno));
    // Set socket option SO_REUSEADDR = true
    int opt_val = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &opt_val, sizeof(opt_val)) < 0)
        errexit("can't socket option: %s\n", strerror(errno));
    // Bind the socket
    if (bind(sock, (struct sockaddr *)&sin, sizeof(sin)) < 0)
        errexit("can't bind to %d port: %s\n", port_num, strerror(errno));
    if (listen(sock, qlen) < 0)
        errexit("can't listen on %d port: %s\n", port_num, strerror(errno));
    return sock;
}
