#include <iostream>
#include <sstream>
#include <string>
#include <array>
#include <vector>
#include <map>
#include <algorithm>

#include <cstring>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>

#include "serversocket.hpp"

#define MAX_USER 30

#define LOGIN  1
#define LOGOUT 2
#define NAME   3
#define YELL   4
#define TELL   5
#define SEND   6
#define RECV   7

using namespace std;

struct user_info {
    int user_id;
    string user_name;
    string ip;
    int port;
    vector<string> env_strs;
    vector<array<int, 3>> p_list;
};
map<int, user_info *> info_list;  // user's info : info_list[socket] = user_info
int current_sock;                 // current socket
vector<array<int, 3>>* pipe_list; // current pipe list : [pipe_count, read-end, write-end]

array<int, MAX_USER> uid_to_sock; // uid_to_sock[uid] = socket, or -1 means not used yet
int user_count;                   // number of users

string current_cmd_line;          // whole line of current command
bool cmd_end = false;

// user_pipe[source_uid][target_uid] = <read-end, write-end>
array<array<array<int, 2>, MAX_USER>, MAX_USER> user_pipe;

void print_welcome() {
    cout << "****************************************" << endl;
    cout << "** Welcome to the information server. **" << endl;
    cout << "****************************************" << endl;
}

void broadcast(int sock, int type, string msg = "", int target_sock = 0) {
    string u_name = info_list[sock]->user_name;
    if (u_name == "") {
        u_name = "(no name)";
    }
    string u_ip   = info_list[sock]->ip;
    int    u_port = info_list[sock]->port;
    int    u_id   = info_list[sock]->user_id;

    string t_name;
    int    t_id;

    switch (type) {
        case LOGIN:
            for (int s : uid_to_sock) {
                if (s != -1) {
                    dprintf(s, "*** User '%s' entered from %s:%d. ***\n",
                            u_name.c_str(), u_ip.c_str(), u_port);
                }
            }
            break;
        case LOGOUT:
            for (int s : uid_to_sock) {
                if (s != -1) {
                    dprintf(s, "*** User '%s' left. ***\n", u_name.c_str());
                }
            }
            break;
        case NAME:
            for (int s : uid_to_sock) {
                if (s != -1) {
                    dprintf(s, "*** User from %s:%d is named '%s'. ***\n",
                            u_ip.c_str(), u_port, u_name.c_str());
                }
            }
            break;
        case YELL:
            for (int s : uid_to_sock) {
                if (s != -1) {
                    dprintf(s, "*** %s yelled ***: %s\n", u_name.c_str(), msg.c_str());
                }
            }
            break;
        case TELL:
            dprintf(target_sock, "*** %s told you ***: %s\n", u_name.c_str(), msg.c_str());
            break;
        case SEND:
            t_name = info_list[target_sock]->user_name;
            if (t_name == "") {
                t_name = "(no name)";
            }
            t_id   = info_list[target_sock]->user_id;
            for (int s : uid_to_sock) {
                if (s != -1) {
                    dprintf(s, "*** %s (#%d) just piped '%s' to %s (#%d) ***\n",
                            u_name.c_str(), u_id + 1, msg.c_str(), t_name.c_str(), t_id + 1);
                }
            }
            break;
        case RECV:
            t_name = info_list[target_sock]->user_name;
            if (t_name == "") {
                t_name = "(no name)";
            }
            t_id   = info_list[target_sock]->user_id;
            for (int s : uid_to_sock) {
                if (s != -1) {
                    dprintf(s, "*** %s (#%d) just received from %s (#%d) by '%s' ***\n",
                            u_name.c_str(), u_id + 1, t_name.c_str(), t_id + 1, msg.c_str());
                }
            }
            break;
        default:
            cerr << "broadcast failed" << endl;
            return;
    }
    cout << flush;
    return;
}

void childHandler(int signo) {
    int status;
    while (waitpid(-1, &status, WNOHANG) > 0) {
        // do nothing
    }
}

char *new_cstr_ptr(const string& s) {
    char *tmp = new char[s.size() + 1];
    strcpy(tmp, s.c_str());
    return tmp;
}

vector<vector<string>> split_commands(string input) {
    vector<vector<string>> cmds;
    vector<string> splited;
    stringstream ss(input);
    while (ss >> input) {
        splited.push_back(input);
        if (input[0] == '|' || input[0] == '!') {
            cmds.push_back(splited);
            splited.clear();
        }
    }
    if (!splited.empty()) {
        cmds.push_back(splited);
    }
    return cmds;
}

int execute(vector<string> args) {
    string check_pipe = args.back();
    int pipe_type = 0, pipe_count = 0;
    int fd[2], u_pipe_fd[2];
    string file_name = ""; // empty-string means not used
    int source_uid = -1, target_uid = -1; // -1 means not used
    string user_pipe_msg = "";

    if (check_pipe[0] == '|') {
        pipe_type = 1;
    }
    if (check_pipe[0] == '!') {
        pipe_type = 2;
    }

    if (pipe_type > 0) {
        args.pop_back();
        string pipe_count_str = check_pipe.substr(1);
        bool has_plus = false;
        for (int i = 0; i < pipe_count_str.size(); i++) {
            if (pipe_count_str[i] == '+') {
                has_plus = true;
                pipe_count = stoi(pipe_count_str.substr(0, i)) + stoi(pipe_count_str.substr(i + 1));
                break;
            }
        }
        if (!has_plus) {
            pipe_count = (check_pipe.size() == 1)? 1 : stoi(check_pipe.substr(1));
        }
        bool found = false;
        for (auto info : *pipe_list) {
            if (info[0] == pipe_count) {
                found = true;
                fd[0] = info[1];
                fd[1] = info[2];
                break;
            }
        }
        if (!found) {
            while (pipe(fd) < 0) {
                usleep(100);
            }
            array<int, 3> pipe_info = {pipe_count, fd[0], fd[1]};

            bool inserted = false;
            for (auto it = pipe_list->begin(); it != pipe_list->end(); it++) {
                if ((*it)[0] > pipe_count) {
                    pipe_list->insert(it, pipe_info);
                    inserted = true;
                    break;
                }
            }

            if (!inserted) {
                pipe_list->push_back(pipe_info);
            }
        }
    }

    // parsing '>[uid]' & '<[uid]' & '> [file_path]'
    vector<string> tmp_args(args);
    vector<int> remove_idx;
    remove_idx.clear();
    for (int i = 0; i < tmp_args.size(); i++) {
        if (tmp_args[i].find('>') != string::npos) {
            if (tmp_args[i].size() == 1) {
                // output to file
                if (i + 1 < tmp_args.size()) {
                    file_name = tmp_args[i + 1];
                    remove_idx.push_back(i);
                    remove_idx.push_back(i + 1);
                } else {
                    cerr << "redirection '> [file_path]' failed: missing file_path" << endl;
                    return -1;
                }
            } else {
                // output to target user
                target_uid = stoi(tmp_args[i].substr(1)) - 1;
                remove_idx.push_back(i);
            }
        }
        if (tmp_args[i].find('<') != string::npos) {
            // input from source user
            source_uid = stoi(tmp_args[i].substr(1)) - 1;
            remove_idx.push_back(i);
        }
    }
    args.clear();
    // place back to vector 'args'
    for (int i = 0; i < tmp_args.size(); i++) {
        if (find(remove_idx.begin(), remove_idx.end(), i) == remove_idx.end()) {
            args.push_back(tmp_args[i]);
        }
    }

    if (source_uid != -1) {
        // check to the read-end of user_pipe
        if (uid_to_sock[source_uid] == -1) {
            cerr << "*** Error: user #" << source_uid + 1 << " does not exist yet. ***" << endl;
            return -1;
        } else if (user_pipe[source_uid][info_list[current_sock]->user_id][0] == -1) {
            cerr << "*** Error: the pipe #" << source_uid + 1 << "->#"
                 << info_list[current_sock]->user_id + 1 << " does not exist yet. ***" << endl;
            return -1;
        } else {
            broadcast(current_sock, RECV, current_cmd_line, uid_to_sock[source_uid]);
        }
    }

    if (target_uid != -1) {
        // check to the write-end of user_pipe
        if (uid_to_sock[target_uid] == -1) {
            cerr << "*** Error: user #" << target_uid + 1 << " does not exist yet. ***" << endl;
            return -1;
        } else if (user_pipe[info_list[current_sock]->user_id][target_uid][0] != -1) {
            cerr << "*** Error: the pipe #" << info_list[current_sock]->user_id + 1
                 << "->#" << target_uid + 1 << " already exists. ***" << endl;
            return -1;
        } else {
            while (pipe(u_pipe_fd) < 0) {
                usleep(100);
            }
            user_pipe[info_list[current_sock]->user_id][target_uid][0] = u_pipe_fd[0];
            user_pipe[info_list[current_sock]->user_id][target_uid][1] = u_pipe_fd[1];
            broadcast(current_sock, SEND, current_cmd_line, uid_to_sock[target_uid]);
        }
    }

    vector<const char *> c_args;
    for (auto s : args) {
        c_args.push_back(new_cstr_ptr(s));
    }
    c_args.push_back(nullptr);

    int pid;
    while ((pid = fork()) < 0) {
        usleep(100);
    }   

    if (pid == 0) {
        // Child process
        if (!pipe_list->empty() && pipe_list->front()[0] == 0) {
            // connect to the read end of pipe
            close(pipe_list->front()[2]);
            dup2(pipe_list->front()[1], STDIN_FILENO);
            close(pipe_list->front()[1]);
        }

        if (source_uid != -1) {
            // connect to the read-end of user_pipe
            dup2(user_pipe[source_uid][info_list[current_sock]->user_id][0], STDIN_FILENO);
            close(user_pipe[source_uid][info_list[current_sock]->user_id][0]);
        }

        if (pipe_type > 0) {
            // connect to the write end of pipe
            close(fd[0]);
            dup2(fd[1], STDOUT_FILENO);
            if (pipe_type == 2) {
                dup2(fd[1], STDERR_FILENO);
            }
            close(fd[1]);
        }

        if (file_name != "") {
            // write to file
            int file_fd = open(file_name.c_str(),
                                O_RDWR|O_CREAT|O_TRUNC,
                                S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
            dup2(file_fd, STDOUT_FILENO);
            close(file_fd);
        }

        if (target_uid != -1) {
            // connect to the write-end of user_pipe
            close(u_pipe_fd[0]);
            dup2(u_pipe_fd[1], STDOUT_FILENO);
            close(u_pipe_fd[1]);
        }

        if (execvp(c_args[0], const_cast<char * const *>(c_args.data())) == -1) {
            cerr << "Unknown command: [" << args[0] << "]." << endl;
        }
        exit(EXIT_FAILURE);
    } else {
        // Parent process
        if (!pipe_list->empty() && pipe_list->front()[0] == 0) {
            close(pipe_list->front()[1]);
            close(pipe_list->front()[2]);
            pipe_list->erase(pipe_list->begin());
        }

        if (source_uid != -1) {
            close(user_pipe[source_uid][info_list[current_sock]->user_id][0]);
            user_pipe[source_uid][info_list[current_sock]->user_id][0] = -1;
            user_pipe[source_uid][info_list[current_sock]->user_id][1] = -1;
        }

        if (target_uid != -1) {
            close(u_pipe_fd[1]);
        }

        for (auto ptr : c_args) {
            delete ptr;
        } // Why the pointers in the vector will be deleted automatically ?

        return pid;
    }
}

int exec_command(const vector<string>& args) {
    if (args[0] == "printenv") {
        if (args.size() != 2) {
            cerr << "syntax error, Usage: printenv [variable name]" << endl;
        } else {
            char *env_str = getenv(args[1].c_str());
            if (env_str != NULL) {
                cout << env_str << endl;
            }
        }
    } else if (args[0] == "setenv") {
        if (args.size() != 3) {
            cerr << "syntax error, Usage: setenv [variable name] [value to assign]" << endl;
        } else {
            if (setenv(args[1].c_str(), args[2].c_str(), 1) != 0) {
                cerr << "setenv error: set variable name [" << args[1] << "] failed" << endl;
            }
        }
    } else if (args[0] == "exit") {
        return 0;
    } else if (args[0] == "who") {
        cout << "<ID>\t<nickname>\t<IP:port>\t<indicate me>" << endl;
        string tmp_name, tmp_ip;
        int tmp_port;
        for (int i = 0; i < uid_to_sock.size(); i++) {
            if (uid_to_sock[i] != -1) {
                tmp_name = info_list[uid_to_sock[i]]->user_name;
                if (tmp_name == "") {
                    tmp_name = "(no name)";
                }
                tmp_ip   = info_list[uid_to_sock[i]]->ip;
                tmp_port = info_list[uid_to_sock[i]]->port;
                cout << i + 1    << "\t"
                     << tmp_name << "\t"
                     << tmp_ip   << ":" << tmp_port;
                if (uid_to_sock[i] == current_sock) {
                    cout << "\t <-me";
                }
                cout << endl;
            }
        }
    } else if (args[0] == "name") {
        if (args.size() < 2) {
            cerr << "syntax error, Usage: name [newname]" << endl;
        } else {
            int idx = current_cmd_line.find(args[0]);
            for (idx += args[0].size(); idx < current_cmd_line.size(); idx++) {
                if (current_cmd_line[idx] != ' ' && current_cmd_line[idx] != '\t') {
                    break;
                }
            }
            string tmp_name = current_cmd_line.substr(idx);
            for (int s : uid_to_sock) {
                if (s != -1 && tmp_name == info_list[s]->user_name) {
                    cerr << "*** User '" << tmp_name << "' already exists. ***" << endl;
                    return -1;
                }
            }
            info_list[current_sock]->user_name = tmp_name;
            broadcast(current_sock, NAME);
            cmd_end = true;
        }
    } else if (args[0] == "yell") {
        if (args.size() < 2) {
            cerr << "syntax error, Usage: yell [message]" << endl;
        } else {
            int idx = current_cmd_line.find(args[0]);
            for (idx += args[0].size(); idx < current_cmd_line.size(); idx++) {
                if (current_cmd_line[idx] != ' ' && current_cmd_line[idx] != '\t') {
                    break;
                }
            }
            broadcast(current_sock, YELL, current_cmd_line.substr(idx));
            cmd_end = true;
        }
    } else if (args[0] == "tell") {
        if (args.size() < 3) {
            cerr << "syntax error, Usage: tell [user_id] [message]" << endl;
        } else {
            int target_uid = stoi(args[1]) - 1;
            if (uid_to_sock[target_uid] != -1) {
                int idx = current_cmd_line.find(args[1]);
                for (idx += args[1].size(); idx < current_cmd_line.size(); idx++) {
                    if (current_cmd_line[idx] != ' ' && current_cmd_line[idx] != '\t') {
                        break;
                    }
                }
                broadcast(current_sock, TELL, current_cmd_line.substr(idx), uid_to_sock[target_uid]);
                cmd_end = true;
            } else {
                cerr << "*** Error: user #" << target_uid + 1 << " does not exist yet. ***" << endl;
            }
        }
    } else {
        return execute(args);
    }
    return -1;
}

int shell(void) {
    vector<vector<string>> commands;
    int last_pid;
    string input;
    cin.clear();
    if (getline(cin, input)) {
        if (!(commands = split_commands(input)).empty()) {
            current_cmd_line = input;
            for (auto cmd : commands) {
                if (cmd_end) {
                    break;
                }
                last_pid = exec_command(cmd);
                if (last_pid == 0) {
                    return 0;
                }
                for (auto it = pipe_list->begin(); it != pipe_list->end(); it++) {
                    (*it)[0] -= 1;
                }
            }
            if (last_pid != -1) {
                bool need_to_wait = true;
                vector<string> last_cmd = commands.back();
                for (string s : last_cmd) {
                    if (s.find('|') != string::npos || s.find('!') != string::npos || s.find('>') != string::npos) {
                        need_to_wait = false;
                        break;
                    }
                }

                if (need_to_wait) {
                    int status;
                    waitpid(last_pid, &status, 0);
                }
            }
        }
        cmd_end = false;
        cout << "% " << flush;
        return 1;
    }
    return 0;
}

int main(int argc, char const *argv[]) {
    // preserve origin std in, out, error
    int std_input  = dup(STDIN_FILENO);
    int std_output = dup(STDOUT_FILENO);
    int std_error  = dup(STDERR_FILENO);
    // init global variable
    for (int i = 0; i < uid_to_sock.size(); i++) {
        uid_to_sock[i] = -1;
    }
    user_count = 0;
    for (int i = 0; i < user_pipe.size(); i++) {
        for (int j = 0; j < user_pipe[i].size(); j++) {
            for (int k = 0; k < user_pipe[i][j].size(); k++) {
                user_pipe[i][j][k] = -1;
            }
        }
    }

    signal(SIGCHLD, childHandler);

    struct sockaddr_in fsin; // the from address of a client
    int mport;               // master port number
    int msock;               // master socket
    fd_set read_fdset;       // read file descriptor set
    fd_set act_fdset;        // active file descriptor set

    switch (argc) {
        case 1:
            break;
        case 2:
            mport = atoi(argv[1]);
            break;
        default:
            cerr << "usage: np_single_proc [port]" << endl;
            return EXIT_FAILURE;
    }

    msock = serv_tcp_sock(mport, 30);
    int maxfd = getdtablesize();
    FD_ZERO(&act_fdset);
    FD_SET(msock, &act_fdset);
    while (true) {
        memcpy(&read_fdset, &act_fdset, sizeof(read_fdset));
        if (select(maxfd + 1, &read_fdset, NULL, NULL, NULL) < 0) {
            continue;
        }
        if (FD_ISSET(msock, &read_fdset) && user_count < MAX_USER) {
            // connect from client
            int alen = sizeof(fsin); // from-address length
            int ssock = accept(msock, (struct sockaddr *)&fsin, (socklen_t *)&alen);
            if (ssock < 0) {
                cerr << "accept failed" << endl;
                continue;
            }
            char ip_cstr[INET_ADDRSTRLEN];
            inet_ntop(AF_INET, &(fsin.sin_addr), ip_cstr, INET_ADDRSTRLEN);
            FD_SET(ssock, &act_fdset);

            // create user info & update user count
            info_list[ssock] = new user_info;
            for (int i = 0; i < uid_to_sock.size(); i++) {
                if (uid_to_sock[i] == -1) {
                    info_list[ssock]->user_id = i;
                    uid_to_sock[i] = ssock;
                    break;
                }
            }
            info_list[ssock]->user_name = "";
            info_list[ssock]->ip = ip_cstr;
            info_list[ssock]->port = (int)ntohs(fsin.sin_port);
            info_list[ssock]->env_strs.push_back("PATH=bin:.");
            info_list[ssock]->p_list.clear();
            user_count++;

            dup2(ssock, STDIN_FILENO);
            dup2(ssock, STDOUT_FILENO);
            dup2(ssock, STDERR_FILENO);
            print_welcome();
            broadcast(ssock, LOGIN);
            cout << "% " << flush;
        }
        for (int fd = 0; fd < maxfd; ++fd) {
            if (fd != msock && FD_ISSET(fd, &read_fdset)) {
                clearenv();
                for (string s : info_list[fd]->env_strs) {
                    string key = s.substr(0, s.find('='));
                    string value = s.substr(s.find('=') + 1);
                    if (setenv(key.c_str(), value.c_str(), 1) != 0) {
                        cerr << "setenv error: set variable name [" << key << "] failed" << endl;
                    }
                }
                current_sock = fd;
                pipe_list = &(info_list[fd]->p_list);
                dup2(fd, STDIN_FILENO);
                dup2(fd, STDOUT_FILENO);
                dup2(fd, STDERR_FILENO);
                if (shell() == 0) {
                    broadcast(fd, LOGOUT);
                    // close all user_pipe send in from other users
                    for (int i = 0; i < user_pipe.size(); i++) {
                        close(user_pipe[i][info_list[fd]->user_id][0]);
                        user_pipe[i][info_list[fd]->user_id][0] = -1;
                        user_pipe[i][info_list[fd]->user_id][1] = -1;
                    }
                    // close all user_pipe send out to other users
                    for (int i = 0; i < user_pipe.size(); i++) {
                        close(user_pipe[info_list[fd]->user_id][i][0]);
                        user_pipe[info_list[fd]->user_id][i][0] = -1;
                        user_pipe[info_list[fd]->user_id][i][1] = -1;
                    }
                    // close server-to-client socket
                    close(fd);
                    FD_CLR(fd, &act_fdset);
                    // remove user info
                    uid_to_sock[info_list[fd]->user_id] = -1;
                    user_count--;
                    for (auto p : *pipe_list) {
                        close(p[1]);
                        close(p[2]);
                    }
                    delete info_list[fd];
                } else {
                    // backup environ to info_list[fd]
                    info_list[fd]->env_strs.clear();
                    for (char **env = environ; *env != 0; env++) {
                        char *env_str = *env;
                        info_list[fd]->env_strs.emplace_back(env_str);
                    }
                }
            }
        }
        // clean
        dup2(std_input,  STDIN_FILENO);
        dup2(std_output, STDOUT_FILENO);
        dup2(std_error,  STDERR_FILENO);
    }
    return EXIT_SUCCESS;
}
