#include <iostream>
#include <sstream>
#include <string>
#include <array>
#include <vector>

#include <cstring>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>

#include "serversocket.hpp"

using namespace std;

vector<array<int, 3>> pipe_list; // [pipe_count, read-end, write-end]

void childHandler(int signo) {
    int status;
    while (waitpid(-1, &status, WNOHANG) > 0) {
        // do nothing
    }
}

char *new_cstr_ptr(const string& s) {
    char *tmp = new char[s.size() + 1];
    strcpy(tmp, s.c_str());
    return tmp;
}

vector<vector<string>> split_commands(string input) {
    vector<vector<string>> cmds;
    vector<string> splited;
    stringstream ss(input);
    while (ss >> input) {
        splited.push_back(input);
        if (input[0] == '|' || input[0] == '!') {
            cmds.push_back(splited);
            splited.clear();
        }
    }
    if (!splited.empty()) {
        cmds.push_back(splited);
    }
    return cmds;
}

int execute(vector<string> args) {
    string check_pipe = args.back();
    int pipe_type = 0, pipe_count = 0;
    int fd[2];
    string file_name = "";

    if (check_pipe[0] == '|') {
        pipe_type = 1;
    }
    if (check_pipe[0] == '!') {
        pipe_type = 2;
    }

    if (pipe_type > 0) {
        args.pop_back();
        string pipe_count_str = check_pipe.substr(1);
        bool has_plus = false;
        for (int i = 0; i < pipe_count_str.size(); i++) {
            if (pipe_count_str[i] == '+') {
                has_plus = true;
                pipe_count = stoi(pipe_count_str.substr(0, i)) + stoi(pipe_count_str.substr(i + 1));
                break;
            }
        }
        if (!has_plus) {
            pipe_count = (check_pipe.size() == 1)? 1 : stoi(check_pipe.substr(1));
        }
        bool found = false;
        for (auto info : pipe_list) {
            if (info[0] == pipe_count) {
                found = true;
                fd[0] = info[1];
                fd[1] = info[2];
                break;
            }
        }
        if (!found) {
            while (pipe(fd) < 0) {
                usleep(100);
            }
            array<int, 3> pipe_info = {pipe_count, fd[0], fd[1]};

            bool inserted = false;
            for (auto it = pipe_list.begin(); it != pipe_list.end(); it++) {
                if ((*it)[0] > pipe_count) {
                    pipe_list.insert(it, pipe_info);
                    inserted = true;
                    break;
                }
            }

            if (!inserted) {
                pipe_list.push_back(pipe_info);
            }
        }
    } else {
        for (int i = 0; i < args.size(); i++) {
            if (args[i] == ">") {
                file_name = args[i + 1];
                while (args.size() > i) {
                    args.pop_back();
                }
                break;
            }
        }
    }

    vector<const char *> c_args;
    for (auto s : args) {
        c_args.push_back(new_cstr_ptr(s));
    }
    c_args.push_back(nullptr);

    int pid;
    while ((pid = fork()) < 0) {
        usleep(100);
    }

    if (pid == 0) {
        // Child process
        if (!pipe_list.empty() && pipe_list.front()[0] == 0) {
            // connect to the read end of pipe
            close(pipe_list.front()[2]);
            dup2(pipe_list.front()[1], STDIN_FILENO);
            close(pipe_list.front()[1]);
        }

        if (pipe_type > 0) {
            // connect to the write end of pipe
            close(fd[0]);
            dup2(fd[1], STDOUT_FILENO);
            if (pipe_type == 2) {
                dup2(fd[1], STDERR_FILENO);
            }
            close(fd[1]);
        } else {
            if (file_name != "") {
                // write to file
                int file_fd = open(file_name.c_str(),
                                   O_RDWR|O_CREAT|O_TRUNC,
                                   S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
                dup2(file_fd, STDOUT_FILENO);
                close(file_fd);
            }
        }

        if (execvp(c_args[0], const_cast<char * const *>(c_args.data())) == -1) {
            cerr << "Unknown command: [" << args[0] << "]." << endl;
        }
        exit(EXIT_FAILURE);
    } else {
        // Parent process
        if (!pipe_list.empty() && pipe_list.front()[0] == 0) {
            close(pipe_list.front()[1]);
            close(pipe_list.front()[2]);
            pipe_list.erase(pipe_list.begin());
        }

        for (auto ptr : c_args) {
            delete ptr;
        } // Why the pointers in the vector will be deleted automatically ?

        return pid;
    }
}

int exec_command(const vector<string>& args) {
    if (args[0] == "printenv") {
        if (args.size() != 2) {
            cerr << "syntax error, Usage: printenv [variable name]" << endl;
        } else {
            char *env_str = getenv(args[1].c_str());
            if (env_str != NULL) {
                cout << env_str << endl;
            }
        }
    } else if (args[0] == "setenv") {
        if (args.size() != 3) {
            cerr << "syntax error, Usage: setenv [variable name] [value to assign]" << endl;
        } else {
            if (setenv(args[1].c_str(), args[2].c_str(), 1) != 0) {
                cerr << "setenv error: variable name [" << args[1] << "] not found" << endl;
            }
        }
    } else if (args[0] == "exit") {
        return 0;
    } else {
        return execute(args);
    }
    return -1;
}

void shell_loop(void) {
    vector<vector<string>> commands;
    int last_pid;
    string input;
    cin.clear();
    while ((cout << "% " << flush) && getline(cin, input)) {
        if ((commands = split_commands(input)).empty()) {
            continue;
        }
        for (auto cmd : commands) {
            last_pid = exec_command(cmd);
            if (last_pid == 0) {
                return;
            }
            for (auto it = pipe_list.begin(); it != pipe_list.end(); it++) {
                (*it)[0] -= 1;
            }
        }
        if (last_pid != -1) {
            string end_of_line = commands.back().back();
            if (end_of_line[0] != '|' && end_of_line[0] != '!') {
                int status;
                waitpid(last_pid, &status, 0);
            }
        }
    }
}

int main(int argc, char const *argv[]) {
    // preserve std in, out, error
    int std_input  = dup(STDIN_FILENO);
    int std_output = dup(STDOUT_FILENO);
    int std_error  = dup(STDERR_FILENO);

    signal(SIGCHLD, childHandler);
    struct sockaddr_in fsin; // the from address of a client
    int port;                // service name or port number
    int msock, ssock;        // master & slave sockets
    int alen;                // from-address length
    switch (argc) {
        case 1:
            break;
        case 2:
            port = atoi(argv[1]);
            break;
        default:
            errexit("usage: np_simple [port]\n");
    }
    msock = serv_tcp_sock(port, 20);
    while (true) {
        // connect from client
        alen = sizeof(fsin);
        ssock = accept(msock, (struct sockaddr *)&fsin, (socklen_t *)&alen);
        if (ssock < 0) {
            errexit("accept failed: %s\n", sys_errlist[errno]);
        }
        // init
        dup2(ssock, STDIN_FILENO);
        dup2(ssock, STDOUT_FILENO);
        dup2(ssock, STDERR_FILENO);
        close(ssock);
        clearenv();
        if (setenv("PATH", "bin:.", 1) != 0) {
            cerr << "npshell init error" << endl;
            return EXIT_FAILURE;
        }
        // shell main lop
        shell_loop();
        // clean
        dup2(std_input,  STDIN_FILENO);
        dup2(std_output, STDOUT_FILENO);
        dup2(std_error,  STDERR_FILENO);
        pipe_list.clear();
    }
    return EXIT_SUCCESS;
}
