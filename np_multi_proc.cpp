#include <iostream>
#include <sstream>
#include <string>
#include <array>
#include <vector>
#include <algorithm>

#include <cstring>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "serversocket.hpp"

#define MAX_USER    30
#define MAX_MSG_LEN 2000

#define LOGIN  1
#define LOGOUT 2
#define NAME   3
#define YELL   4
#define TELL   5
#define SEND   6
#define RECV   7
#define SHUTDW 8
#define CANCEL 9

using namespace std;

// info_list[user_id] = user_info of the user whos uid == user_id
struct user_info {
    int user_pid; // -1 if not used
    char user_name[100];
    char ip[INET_ADDRSTRLEN];
    int port;
};
user_info *user_info_ptr;
int        user_info_shmkey;
int        user_info_shmid;

int master_pid;
bool to_shutdown;
time_t shutdown_time;

char *msg_ptr;
int   msg_shmkey;
int   msg_shmid;

int curr_user_id;   // current user id
int user_count = 0; // user count

vector<array<int, 3>> pipe_list; // current pipe list : [pipe_count, read-end, write-end]

string current_cmd_line;         // whole line of current command
bool cmd_end = false;

struct user_pipe {
    // user_pipe[source_uid][target_uid] = <read-end, write-end>
    int u_p_table[MAX_USER][MAX_USER][2];
    // id of the user who just send SIGUSER2 by kill()
    int last_kill_uid;
};
user_pipe *u_pipe_ptr;
int        u_pipe_shmkey;
int        u_pipe_shmid;

void print_welcome() {
    cout << "****************************************" << endl;
    cout << "** Welcome to the information server. **" << endl;
    cout << "****************************************" << endl;
}

void broadcast(int self_uid, int type, string msg = "", int target_uid = 0) {
    string u_name(user_info_ptr[self_uid].user_name);
    if (u_name == "") {
        u_name = "(no name)";
    }
    string u_ip   = user_info_ptr[self_uid].ip;
    int    u_port = user_info_ptr[self_uid].port;
    int    u_id   = self_uid;

    string t_name;
    int    t_id;

    switch (type) {
        case LOGIN:
            sprintf(msg_ptr, "*** User '%s' entered from %s:%d. ***",
                    u_name.c_str(), u_ip.c_str(), u_port);
            break;
        case LOGOUT:
            sprintf(msg_ptr, "*** User '%s' left. ***", u_name.c_str());
            break;
        case NAME:
            sprintf(msg_ptr, "*** User from %s:%d is named '%s'. ***",
                    u_ip.c_str(), u_port, u_name.c_str());
            break;
        case YELL:
            sprintf(msg_ptr, "*** %s yelled ***: %s", u_name.c_str(), msg.c_str());
            break;
        case TELL:
            sprintf(msg_ptr, "*** %s told you ***: %s", u_name.c_str(), msg.c_str());
            cout << flush;
            kill(user_info_ptr[target_uid].user_pid, SIGUSR1);
            return;
        case SEND:
            t_name = user_info_ptr[target_uid].user_name;
            if (t_name == "") {
                t_name = "(no name)";
            }
            t_id   = target_uid;
            sprintf(msg_ptr, "*** %s (#%d) just piped '%s' to %s (#%d) ***",
                    u_name.c_str(), u_id + 1, msg.c_str(), t_name.c_str(), t_id + 1);
            break;
        case RECV:
            t_name = user_info_ptr[target_uid].user_name;
            if (t_name == "") {
                t_name = "(no name)";
            }
            t_id   = target_uid;
            sprintf(msg_ptr, "*** %s (#%d) just received from %s (#%d) by '%s' ***",
                    u_name.c_str(), u_id + 1, t_name.c_str(), t_id + 1, msg.c_str());
            break;
        case SHUTDW:
            strcpy(msg_ptr, "shutdown");
            kill(master_pid, SIGUSR1);
            return;
        case CANCEL:
            strcpy(msg_ptr, "cancel");
            kill(master_pid, SIGUSR1);
            return;
        default:
            cerr << "broadcast failed" << endl;
            return;
    }
    // broadcast
    cout << flush;
    for (int i = 0; i < MAX_USER; i++) {
        if (user_info_ptr[i].user_pid != 0) {
            kill(user_info_ptr[i].user_pid, SIGUSR1);
        }
    }
    return;
}

void print_user_list(user_info *ptr, int self_id) {
    cout << "<ID>\t<nickname>\t<IP:port>\t<indicate me>" << endl;
    string tmp_name, tmp_ip;
    int tmp_port;
    for (int i = 0; i < MAX_USER; i++) {
        if (ptr[i].user_pid != 0) {
            tmp_name = ptr[i].user_name;
            if (tmp_name == "") {
                tmp_name = "(no name)";
            }
            tmp_ip   = ptr[i].ip;
            tmp_port = ptr[i].port;
            cout << i + 1    << "\t"
                 << tmp_name << "\t"
                 << tmp_ip   << ":" << tmp_port;
            if (i == self_id) {
                cout << "\t <-me";
            }
            cout << endl;
        }
    }
}

void child_handler(int signum) {
    int status;
    while (waitpid(-1, &status, WNOHANG) > 0) {
        // do nothing
    }
}

void msg_handler(int signum) {
    if (signum == SIGUSR1) {
        if (getpid() == master_pid) {
            string cmd = msg_ptr;
            if (cmd == "shutdown") {
                to_shutdown = true;
                shutdown_time = time(NULL);
                strcpy(msg_ptr, " === server will shutdown in 5 second. ===");
                for (int i = 0; i < MAX_USER; i++) {
                    if (user_info_ptr[i].user_pid != 0) {
                        kill(user_info_ptr[i].user_pid, SIGUSR1);
                    }
                }
            } else if (cmd == "cancel") {
                to_shutdown = false;
                strcpy(msg_ptr, " === server shutdown has been canceled. ===");
                for (int i = 0; i < MAX_USER; i++) {
                    if (user_info_ptr[i].user_pid != 0) {
                        kill(user_info_ptr[i].user_pid, SIGUSR1);
                    }
                }
            }
        } else {
            string msg = msg_ptr;
            if (msg != "") {
                cout << msg << endl;
            } else {
                exit(EXIT_SUCCESS);
            }
        }
    } else {
        cerr << "msg_handler() failed: signum != SIGUSR1" << endl;
    }
}

void user_pipe_handler(int signum) {
    if (signum == SIGUSR2) {
        int target_uid = u_pipe_ptr->last_kill_uid;
        if (u_pipe_ptr->u_p_table[target_uid][curr_user_id][1] != -1) {
            if (u_pipe_ptr->u_p_table[target_uid][curr_user_id][0] != -1) {
                close(u_pipe_ptr->u_p_table[target_uid][curr_user_id][0]);
                u_pipe_ptr->u_p_table[target_uid][curr_user_id][0] = -1;
                u_pipe_ptr->u_p_table[target_uid][curr_user_id][1] = -1;
            }
        } else {
            char fifo_path[100];
            sprintf(fifo_path, "user_pipe/%d-%d", target_uid, curr_user_id);
            u_pipe_ptr->u_p_table[target_uid][curr_user_id][0] = open(fifo_path, O_RDONLY);
        }
    } else {
        cerr << "user_pipe_handler() failed: signum != SIGUSR2" << endl;
    }
}

void master_exit_handler(int signum) {
    shmdt((void*)user_info_ptr);
    shmctl(user_info_shmid, IPC_RMID, NULL);
    shmdt((void*)msg_ptr);
    shmctl(msg_shmid, IPC_RMID, NULL);
    shmdt((void*)u_pipe_ptr);
    shmctl(u_pipe_shmid, IPC_RMID, NULL);
    exit(EXIT_SUCCESS);
}

void slave_exit_handler(int signum) {
    int status, pid;
    while ((pid = waitpid(-1, &status, WNOHANG)) == 0) {
        // do nothing
    }
    for (int i = 0; i < MAX_USER; i++) {
        if (user_info_ptr[i].user_pid == pid) {
            user_info_ptr[i].user_pid = 0;
        }
    }
    user_count--;
}

char *new_cstr_ptr(const string& s) {
    char *tmp = new char[s.size() + 1];
    strcpy(tmp, s.c_str());
    return tmp;
}

vector<vector<string>> split_commands(string input) {
    vector<vector<string>> cmds;
    vector<string> splited;
    stringstream ss(input);
    while (ss >> input) {
        splited.push_back(input);
        if (input[0] == '|' || input[0] == '!') {
            cmds.push_back(splited);
            splited.clear();
        }
    }
    if (!splited.empty()) {
        cmds.push_back(splited);
    }
    return cmds;
}

int execute(vector<string> args) {
    string check_pipe = args.back();
    int pipe_type = 0, pipe_count = 0;
    int fd[2], u_pipe_fd[2]; // [read-end, write-end];
    string file_name = "";
    int source_uid = -1, target_uid = -1; // -1 means not used

    if (check_pipe[0] == '|') {
        pipe_type = 1;
    }
    if (check_pipe[0] == '!') {
        pipe_type = 2;
    }

    if (pipe_type > 0) {
        args.pop_back();
        string pipe_count_str = check_pipe.substr(1);
        bool has_plus = false;
        for (int i = 0; i < pipe_count_str.size(); i++) {
            if (pipe_count_str[i] == '+') {
                has_plus = true;
                pipe_count = stoi(pipe_count_str.substr(0, i)) + stoi(pipe_count_str.substr(i + 1));
                break;
            }
        }
        if (!has_plus) {
            pipe_count = (check_pipe.size() == 1)? 1 : stoi(check_pipe.substr(1));
        }
        bool found = false;
        for (auto info : pipe_list) {
            if (info[0] == pipe_count) {
                found = true;
                fd[0] = info[1];
                fd[1] = info[2];
                break;
            }
        }
        if (!found) {
            while (pipe(fd) < 0) {
                usleep(100);
            }
            array<int, 3> pipe_info = {pipe_count, fd[0], fd[1]};

            bool inserted = false;
            for (auto it = pipe_list.begin(); it != pipe_list.end(); it++) {
                if ((*it)[0] > pipe_count) {
                    pipe_list.insert(it, pipe_info);
                    inserted = true;
                    break;
                }
            }

            if (!inserted) {
                pipe_list.push_back(pipe_info);
            }
        }
    }

    // parsing '>[uid]' & '<[uid]' & '> [file_path]'
    vector<string> tmp_args(args);
    vector<int> remove_idx;
    remove_idx.clear();
    for (int i = 0; i < tmp_args.size(); i++) {
        if (tmp_args[i].find('>') != string::npos) {
            if (tmp_args[i].size() == 1) {
                // output to file
                if (i + 1 < tmp_args.size()) {
                    file_name = tmp_args[i + 1];
                    remove_idx.push_back(i);
                    remove_idx.push_back(i + 1);
                } else {
                    cerr << "redirection '> [file_path]' failed: missing file_path" << endl;
                    return -1;
                }
            } else {
                // output to target user
                target_uid = stoi(tmp_args[i].substr(1)) - 1;
                remove_idx.push_back(i);
            }
        }
        if (tmp_args[i].find('<') != string::npos) {
            // input from source user
            source_uid = stoi(tmp_args[i].substr(1)) - 1;
            remove_idx.push_back(i);
        }
    }
    args.clear();
    // place back to vector 'args'
    for (int i = 0; i < tmp_args.size(); i++) {
        if (find(remove_idx.begin(), remove_idx.end(), i) == remove_idx.end()) {
            args.push_back(tmp_args[i]);
        }
    }

    if (source_uid != -1) {
        // check to the read-end of user_pipe
        if (user_info_ptr[source_uid].user_pid == 0) {
            cerr << "*** Error: user #" << source_uid + 1 << " does not exist yet. ***" << endl;
            return -1;
        } else if (u_pipe_ptr->u_p_table[source_uid][curr_user_id][1] == -1) {
            cerr << "*** Error: the pipe #" << source_uid + 1 << "->#"
                 << curr_user_id + 1 << " does not exist yet. ***" << endl;
            return -1;
        } else {
            u_pipe_fd[0] = u_pipe_ptr->u_p_table[source_uid][curr_user_id][0];
            broadcast(curr_user_id, RECV, current_cmd_line, source_uid);
        }
    }

    if (target_uid != -1) {
        // check to the write-end of user_pipe
        if (user_info_ptr[target_uid].user_pid == 0) {
            cerr << "*** Error: user #" << target_uid + 1 << " does not exist yet. ***" << endl;
            return -1;
        } else if (u_pipe_ptr->u_p_table[curr_user_id][target_uid][1] != -1) {
            cerr << "*** Error: the pipe #" << curr_user_id + 1
                 << "->#" << target_uid + 1 << " already exists. ***" << endl;
            return -1;
        } else {
            char fifo_path[100];
            sprintf(fifo_path, "user_pipe/%d-%d", curr_user_id, target_uid);
            mkfifo(fifo_path, 0666);
            u_pipe_ptr->last_kill_uid = curr_user_id;
            kill(user_info_ptr[target_uid].user_pid, SIGUSR2);
            u_pipe_fd[1] = open(fifo_path, O_WRONLY);
            u_pipe_ptr->u_p_table[curr_user_id][target_uid][1] = u_pipe_fd[1];
            broadcast(curr_user_id, SEND, current_cmd_line, target_uid);
        }
    }

    vector<const char *> c_args;
    for (auto s : args) {
        c_args.push_back(new_cstr_ptr(s));
    }
    c_args.push_back(nullptr);

    int pid;
    while ((pid = fork()) < 0) {
        usleep(100);
    }

    if (pid == 0) {
        // Child process
        if (!pipe_list.empty() && pipe_list.front()[0] == 0) {
            // connect to the read end of pipe
            close(pipe_list.front()[2]);
            dup2(pipe_list.front()[1], STDIN_FILENO);
            close(pipe_list.front()[1]);
        }

        if (source_uid != -1) {
            // connect to the read-end of user_pipe
            dup2(u_pipe_fd[0], STDIN_FILENO);
            close(u_pipe_fd[0]);
        }

        if (pipe_type > 0) {
            // connect to the write end of pipe
            close(fd[0]);
            dup2(fd[1], STDOUT_FILENO);
            if (pipe_type == 2) {
                dup2(fd[1], STDERR_FILENO);
            }
            close(fd[1]);
        }

        if (file_name != "") {
            // write to file
            int file_fd = open(file_name.c_str(),
                                O_RDWR|O_CREAT|O_TRUNC,
                                S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
            dup2(file_fd, STDOUT_FILENO);
            close(file_fd);
        }

        if (target_uid != -1) {
            // connect to the write-end of user_pipe
            dup2(u_pipe_fd[1], STDOUT_FILENO);
            close(u_pipe_fd[1]);
        }

        if (execvp(c_args[0], const_cast<char * const *>(c_args.data())) == -1) {
            cerr << "Unknown command: [" << args[0] << "]." << endl;
        }
        exit(EXIT_FAILURE);
    } else {
        // Parent process
        if (!pipe_list.empty() && pipe_list.front()[0] == 0) {
            close(pipe_list.front()[1]);
            close(pipe_list.front()[2]);
            pipe_list.erase(pipe_list.begin());
        }

        if (source_uid != -1) {
            close(u_pipe_ptr->u_p_table[source_uid][curr_user_id][0]);
            u_pipe_ptr->u_p_table[source_uid][curr_user_id][0] = -1;
            u_pipe_ptr->u_p_table[source_uid][curr_user_id][1] = -1;
        }

        if (target_uid != -1) {
            close(u_pipe_ptr->u_p_table[curr_user_id][target_uid][1]);
        }

        for (auto ptr : c_args) {
            delete ptr;
        } // Why the pointers in the vector will be deleted automatically ?

        return pid;
    }
}

int exec_command(const vector<string>& args) {
    if (args[0] == "printenv") {
        if (args.size() != 2) {
            cerr << "syntax error, Usage: printenv [variable name]" << endl;
        } else {
            char *env_str = getenv(args[1].c_str());
            if (env_str != NULL) {
                cout << env_str << endl;
            }
        }
    } else if (args[0] == "setenv") {
        if (args.size() != 3) {
            cerr << "syntax error, Usage: setenv [variable name] [value to assign]" << endl;
        } else {
            if (setenv(args[1].c_str(), args[2].c_str(), 1) != 0) {
                cerr << "setenv error: variable name [" << args[1] << "] not found" << endl;
            }
        }
    } else if (args[0] == "exit") {
        return 0;
    } else if (args[0] == "who") {
        print_user_list(user_info_ptr, curr_user_id);
    } else if (args[0] == "name") {
        if (args.size() < 2) {
            cerr << "syntax error, Usage: name [newname], with lenght of newname < 100" << endl;
        } else {
            int idx = current_cmd_line.find(args[0]);
            for (idx += args[0].size(); idx < current_cmd_line.size(); idx++) {
                if (current_cmd_line[idx] != ' ' && current_cmd_line[idx] != '\t') {
                    break;
                }
            }
            string tmp_name = current_cmd_line.substr(idx);
            if (tmp_name.size() >= 100) {
                cerr << "syntax error, Usage: name [newname], with lenght of newname < 100" << endl;
                return -1;
            }
            for (int i = 0; i < MAX_USER; i++) {
                if (user_info_ptr[i].user_pid != 0 && tmp_name == user_info_ptr[i].user_name) {
                    cerr << "*** User '" << tmp_name << "' already exists. ***" << endl;
                    return -1;
                }
            }
            strcpy(user_info_ptr[curr_user_id].user_name, tmp_name.c_str());
            broadcast(curr_user_id, NAME);
            cmd_end = true;
        }
    } else if (args[0] == "yell") {
        if (args.size() < 2) {
            cerr << "syntax error, Usage: yell [message]" << endl;
        } else {
            int idx = current_cmd_line.find(args[0]);
            for (idx += args[0].size(); idx < current_cmd_line.size(); idx++) {
                if (current_cmd_line[idx] != ' ' && current_cmd_line[idx] != '\t') {
                    break;
                }
            }
            broadcast(curr_user_id, YELL, current_cmd_line.substr(idx));
            cmd_end = true;
        }
    } else if (args[0] == "tell") {
        if (args.size() < 3) {
            cerr << "syntax error, Usage: tell [user_id] [message]" << endl;
        } else {
            int target_uid = stoi(args[1]) - 1;
            if (user_info_ptr[target_uid].user_pid != 0) {
                int idx = current_cmd_line.find(args[1]);
                for (idx += args[1].size(); idx < current_cmd_line.size(); idx++) {
                    if (current_cmd_line[idx] != ' ' && current_cmd_line[idx] != '\t') {
                        break;
                    }
                }
                broadcast(curr_user_id, TELL, current_cmd_line.substr(idx), target_uid);
                cmd_end = true;
            } else {
                cerr << "*** Error: user #" << target_uid + 1 << " does not exist yet. ***" << endl;
            }
        }
    } else if (args[0] == "shutdown") {
        broadcast(curr_user_id, SHUTDW);
    } else if (args[0] == "cancel") {
        broadcast(curr_user_id, CANCEL);
    } else {
        return execute(args);
    }
    return -1;
}

void shell_loop(void) {
    vector<vector<string>> commands;
    int last_pid;
    string input;
    cin.clear();
    while ((cout << "% " << flush) && getline(cin, input)) {
        if ((commands = split_commands(input)).empty()) {
            continue;
        }
        current_cmd_line = input;
        for (auto cmd : commands) {
            if (cmd_end) {
                    break;
            }
            last_pid = exec_command(cmd);
            if (last_pid == 0) {
                return;
            }
            for (auto it = pipe_list.begin(); it != pipe_list.end(); it++) {
                (*it)[0] -= 1;
            }
        }
        if (last_pid != -1) {
            string end_of_line = commands.back().back();
            if (end_of_line[0] != '|' && end_of_line[0] != '!') {
                int status;
                waitpid(last_pid, &status, 0);
            }
        }
        cmd_end = false;
    }
}

int main(int argc, char const *argv[]) {
    // init variables used by shutdown service
    master_pid = getpid();
    to_shutdown = false;
    fd_set act_fdset, read_fdset;
    struct timeval tv;
    tv.tv_sec = 1;
    tv.tv_usec = 0;
    // preserve std in, out, error
    int std_input  = dup(STDIN_FILENO);
    int std_output = dup(STDOUT_FILENO);
    int std_error  = dup(STDERR_FILENO);
    // signal
    signal(SIGINT,  master_exit_handler);
    signal(SIGCHLD, slave_exit_handler);
    signal(SIGUSR1, msg_handler);
    signal(SIGUSR2, user_pipe_handler);
    // get shared memory for storing user_info[MAX_USER]
    user_info_shmkey = 1;
    user_info_shmid  = shmget(user_info_shmkey, sizeof(user_info) * MAX_USER, IPC_CREAT | 0666);
    user_info_ptr    = (user_info *)shmat(user_info_shmid, NULL, 0);
    // init user_info[MAX_USER]
    for (int i = 0; i < MAX_USER; i++) {
        user_info_ptr[i].user_pid = 0;
    }
    // get shared memory for storing msg
    msg_shmkey = 2;
    msg_shmid  = shmget(msg_shmkey, sizeof(char) * MAX_MSG_LEN, IPC_CREAT | 0666);
    msg_ptr    = (char *)shmat(msg_shmid, NULL, 0);
    // get shared memory for user pipe table
    u_pipe_shmkey = 3;
    u_pipe_shmid  = shmget(u_pipe_shmkey, sizeof(user_pipe), IPC_CREAT | 0666);
    u_pipe_ptr    = (user_pipe *)shmat(u_pipe_shmid, NULL, 0);
    // init
    for (int i = 0; i < MAX_USER; i++) {
        for (int j = 0; j < MAX_USER; j++) {
            for (int k = 0; k < 2; k++) {
                u_pipe_ptr->u_p_table[i][j][k] = -1;
            }
        }
    }
    // create master socket
    struct sockaddr_in fsin; // the from address of a client
    int port;                // service name or port number
    int msock, ssock;        // master & slave sockets
    int alen;                // from-address length
    switch (argc) {
        case 1:
            break;
        case 2:
            port = atoi(argv[1]);
            break;
        default:
            cerr << "usage: np_multi_proc [port]" << endl;
            return EXIT_FAILURE;
    }
    msock = serv_tcp_sock(port, 30);
    while (true) {
        // check for shutdown service
        if (to_shutdown && time(NULL) - shutdown_time > 5) {
            strcpy(msg_ptr, " === server shutdown! ===");
            for (int i = 0; i < MAX_USER; i++) {
                if (user_info_ptr[i].user_pid != 0) {
                    kill(user_info_ptr[i].user_pid, SIGUSR1);
                }
            }
            sleep(1);
            strcpy(msg_ptr, "");
            for (int i = 0; i < MAX_USER; i++) {
                if (user_info_ptr[i].user_pid != 0) {
                    kill(user_info_ptr[i].user_pid, SIGUSR1);
                }
            }
            break;
        }
        // use select to void accept() blocking
        int maxfd = getdtablesize();
        FD_ZERO(&act_fdset);
        FD_SET(msock, &act_fdset);
        memcpy(&read_fdset, &act_fdset, sizeof(read_fdset));
        if (select(maxfd + 1, &read_fdset, NULL, NULL, &tv) <= 0) {
            continue;
        }
        // connect from client
        alen = sizeof(fsin);
        while (user_count >= 30) {
            // do nothing
        }
        ssock = accept(msock, (struct sockaddr *)&fsin, (socklen_t *)&alen);
        if (ssock < 0) {
            if (errno == EINTR) {
                continue;
            }
            cerr << "accept failed" << endl;
            return EXIT_FAILURE;
        }
        // get uid, ip, port of new client
        for (curr_user_id = 0; curr_user_id < MAX_USER; curr_user_id++) {
            if (user_info_ptr[curr_user_id].user_pid == 0) {
                break;
            }
        }
        char ip_cstr[INET_ADDRSTRLEN];
        inet_ntop(AF_INET, &(fsin.sin_addr), ip_cstr, INET_ADDRSTRLEN);
        int user_port = (int)ntohs(fsin.sin_port);
        // fork for slave
        int pid;
        while ((pid = fork()) < 0) {
            usleep(100);
        }
        if (pid == 0) {
            // Child process
            close(msock);
            signal(SIGCHLD, child_handler);
            // init
            pipe_list.clear();
            dup2(ssock, STDIN_FILENO);
            dup2(ssock, STDOUT_FILENO);
            dup2(ssock, STDERR_FILENO);
            close(ssock);
            clearenv();
            if (setenv("PATH", "bin:.", 1) != 0) {
                cerr << "npshell init error" << endl;
                return EXIT_FAILURE;
            }
            // shell main lop
            print_welcome();
            while (user_info_ptr[curr_user_id].user_pid == 0) {
                // wait for master update slave pid
            }
            broadcast(curr_user_id, LOGIN);
            shell_loop();
            // clean
            broadcast(curr_user_id, LOGOUT);
            // close all user_pipe send in from other users
            for (int i = 0; i < MAX_USER; i++) {
                close(u_pipe_ptr->u_p_table[i][curr_user_id][0]);
                u_pipe_ptr->u_p_table[i][curr_user_id][0] = -1;
                u_pipe_ptr->u_p_table[i][curr_user_id][1] = -1;
            }
            // close all user_pipe send out to other users
            for (int i = 0; i < MAX_USER; i++) {
                if (u_pipe_ptr->u_p_table[curr_user_id][i][1] != -1) {
                    kill(user_info_ptr[i].user_pid, SIGUSR2);
                }
            }
            pipe_list.clear();
            shmdt((void*)user_info_ptr);
            shmdt((void*)msg_ptr);
            shmdt((void*)u_pipe_ptr);
            return EXIT_SUCCESS;
        } else {
            // Parent process
            close(ssock);
            strcpy(user_info_ptr[curr_user_id].user_name, "");
            strcpy(user_info_ptr[curr_user_id].ip, ip_cstr);
            user_info_ptr[curr_user_id].port = user_port;
            user_info_ptr[curr_user_id].user_pid = pid;
            user_count++;
        }
    }
    master_exit_handler(0);
}
